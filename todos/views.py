from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoItemForm, TodoListForm
# Create your views here.


def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList,  id=id)
    context = {
        "list_detail": list_detail,
    }

    return render(request, "todos/todo_list_detail.html", context)


def todo_list_list(request):
    list_list = TodoList.objects.all()
    context = {
        "list_list": list_list
    }
    return render(request, "todos/todo_list_list.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid:
            todo = form.save()
            todo.save()
        return redirect("todo_list_detail", id = todo.id)
    else:
        form = TodoListForm()

    context = {
        "form":form,
    }
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list_detail)
        if form.is_valid():
            form.save()
        return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list_detail)

    context = {
        "form":form
    }
    return render(request, "todos/todo_list_update.html")

def todo_list_delete(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list_detail.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_list_delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo = form.save()
            todo.save()
        return redirect("todo_list_detail", id = todo.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form":form
    }
    return render(request, "todos/todo_item_create.html", context)


def todo_item_update(request, id):
    item_detail = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance = item_detail)
        if form.is_valid():
            todo = form.save()
            todo.save()
        return redirect("todo_list_detail", id = todo.list.id)
    else:
        form = TodoItemForm(instance = item_detail)

    context = {
        "form":form,
        "item_detail": item_detail,
    }
    return render(request, "todos/todo_item_update.html", context)
